import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_radio_player/flutter_radio_player.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:provider/provider.dart';
import 'package:radio/api_service.dart';
import 'package:radio/screens/acaraTayangScreen.dart';
import 'package:radio/screens/loginScreen.dart';
import 'package:radio/screens/playScreen.dart';

import 'config.dart';
import 'provider/google_sign_in.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  OneSignal.shared.init(Config.oneSignalKey, iOSSettings: {
    OSiOSSettings.autoPrompt: true,
    OSiOSSettings.inAppLaunchUrl: true
  });
  OneSignal.shared
      .setInFocusDisplayType(OSNotificationDisplayType.notification);

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  var playerState = FlutterRadioPlayer.flutter_radio_paused;

  var volume = 0.8;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  APIService apiService;
  FlutterRadioPlayer _flutterRadioPlayer = new FlutterRadioPlayer();
  @override
  void initState() {
    apiService = new APIService();
    OneSignal.shared
        .setNotificationReceivedHandler((OSNotification notification) {});

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      print("notifikasi di tap");
    });
    getConfig();
    getuserOneSignal();
    // initRadioService();
    super.initState();
  }

  Future<void> initRadioService() async {
    try {
      await _flutterRadioPlayer.init(
          "91.6 Mhz", "Live", Config.streamAddress, "false");
    } on PlatformException {
      print("Exception occurred while trying to register the services.");
    }
  }

  Future<void> getuserOneSignal() async {
    var status = await OneSignal.shared.getPermissionSubscriptionState();
    String onesignalUserId = status.subscriptionStatus.userId;
    Config.playerId = onesignalUserId;
  }

  Future<void> getConfig() async {
    await apiService.getConfig().then((value) {
      setState(() {
        Config.streamAddress = value[0].configValue.toString();
        Config.tawktoAddress = value[2].configValue.toString();
        Config.waAddress = value[3].configValue.toString();
        Config.logoRuangDengar = value[4].configValue.toString();
        Config.logoHomeScreen = value[5].configValue.toString();
        Config.noFlayerFound = value[6].configValue.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => GoogleSignInProvider(),
          child: AcaraTayangScreen(),
        ),
      ],
      child: MaterialApp(
        title: "UMSU FM",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: LoginScreen(),
      ),
    );
  }
}
