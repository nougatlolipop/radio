import 'dart:ui';

import 'package:flutter/material.dart';

final Color background = Color(0xFFfafafc);
final Color sliverBackground = Color(0xFFfafafc);
final Color menu1Color = Color(0xFFf9d263);
final Color menu2Color = Color(0xFFfa603d);
final Color menu3Color = Color(0xFF04abe6);
final Color tabVarViewColor = Color(0xFFfdfdfd);
final Color startColor = Color(0xFFfdc746);
final Color subTitleText = Color(0xFFc5c4ca);
final Color loveColor = Color(0xFF00ace6);
final Color audioBulishBackground = Color(0xFFdee7fa);
final Color audioBlueBackground = Color(0xFF04abe7);
final Color audioGreyBackground = Color(0xFFf2f2f2);
