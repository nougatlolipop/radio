class Config {
  static String baseURLApi = 'https://api.umsu.ac.id/';
  static String beritaURL =
      'https://umsufm.com/wp-json/wp/v2/posts?_embed&categories=3';

  static String oneSignalKey = "c98e3edf-b97b-47a4-8d89-103e4b3241d3";
  static String urlAddUser = "radio/addUser";
  static String urlGetConfig = "radio/getConfig";
  static String urlGetAcara = "radio/getAcara";
  static String urlAddFav = "radio/addFav";
  static String urlGetEndors = "radio/getEndorse";
  static String stearmPrambors =
      "https://22283.live.streamtheworld.com/PRAMBORS_FM.mp3";
  static String streamAddress,
      tawktoAddress,
      waAddress,
      logoRuangDengar,
      logoHomeScreen,
      noFlayerFound;
  static String playerId;
}
