import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_arc_speed_dial/flutter_speed_dial_menu_button.dart';
import 'package:flutter_arc_speed_dial/main_menu_floating_action_button.dart';
import 'package:flutter_radio_player/flutter_radio_player.dart';
import 'package:lottie/lottie.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:radio/provider/google_sign_in.dart';
import 'package:radio/screens/allBerita.dart';
import 'package:radio/screens/listEndorsment.dart';
import 'package:radio/screens/loginScreen.dart';
import 'package:radio/screens/requestScreen.dart';
import 'package:radio/widgets/contents.dart';
import 'package:radio/widgets/listBerita.dart';
import 'package:radio/widgets/marquee.dart';
import 'package:radio/app_colors.dart' as AppColors;
import 'package:share/share.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:url_launcher/url_launcher.dart';

import '../api_service.dart';
import '../config.dart';
import '../main.dart';
import 'acaraScreen.dart';

class PlayScreen extends StatefulWidget {
  var playerState = FlutterRadioPlayer.flutter_radio_paused;

  @override
  _PlayScreenState createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  FlutterRadioPlayer _flutterRadioPlayer = new FlutterRadioPlayer();
  GlobalKey _keyRed = GlobalKey();
  RenderBox renderBox;
  double sizeBox = 0;
  double position = 0;
  bool _isShowDial = false;
  final user = FirebaseAuth.instance.currentUser;
  APIService apiService;

  @override
  void initState() {
    super.initState();
    apiService = new APIService();
    user != null
        ? apiService.createUser(user.uid, Config.playerId)
        : print('belum login'); //add pengguna baru
    initLayout();
  }

  Future initLayout() async {
    await new Future.delayed(new Duration(seconds: 2));
    renderBox = _keyRed.currentContext.findRenderObject();
    initRadioService();
    setState(() {
      sizeBox = renderBox.size.height;
      position = renderBox.localToGlobal(Offset.zero).dy;
    });
  }

  Future<void> initRadioService() async {
    try {
      await _flutterRadioPlayer.init(
          "91.6 Mhz", "Live", Config.streamAddress, "false");
    } on PlatformException {
      print("Exception occurred while trying to register the services.");
    }
  }

  // Future<void> initRadioService() async {}

  @override
  Widget build(BuildContext context) {
    int _currentIndex = 0;
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.audioBulishBackground,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            height: screenHeight / 3,
            child: Container(
              color: AppColors.audioBlueBackground,
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: AppBar(
              leading: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  radius: 20,
                  backgroundImage: user != null
                      ? Platform.isAndroid
                          ? NetworkImage(user.photoURL)
                          : AssetImage("asset/img/nouser.png")
                      : AssetImage("asset/img/nouser.png"),
                ),
              ),
              title: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "UMSU FM",
                    ),
                    Visibility(
                      visible: true,
                      child: MarqueeWidget(
                        direction: Axis.horizontal,
                        child: user != null
                            ? Platform.isAndroid
                                ? Text(
                                    user.displayName + " - " + user.email,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontSize: 12.0,
                                    ),
                                  )
                                : Text(
                                    "Kawula Muda - " + user.email,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                      fontSize: 12.0,
                                    ),
                                  )
                            : Text(
                                "Kawula Muda",
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                  fontSize: 12.0,
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
              ),
              actions: [
                user != null
                    ? IconButton(
                        icon: Icon(MdiIcons.power),
                        onPressed: () {
                          SweetAlert.show(context,
                              subtitle: "Do you want to Logout?",
                              style: SweetAlertStyle.confirm,
                              showCancelButton: true,
                              onPress: (bool isConfirm) {
                            if (isConfirm) {
                              SweetAlert.show(context,
                                  subtitle: "Logout...",
                                  style: SweetAlertStyle.loading);
                              Provider.of<GoogleSignInProvider>(context,
                                      listen: false)
                                  .logout();

                              new Future.delayed(new Duration(seconds: 1), () {
                                SweetAlert.show(context,
                                    subtitle: "Success!",
                                    style: SweetAlertStyle.success);
                                _flutterRadioPlayer.stop();
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyApp()));
                              });
                            } else {
                              SweetAlert.show(context,
                                  subtitle: "Canceled!",
                                  style: SweetAlertStyle.error);
                            }
                            // return false to keep dialog
                            return false;
                          });
                        },
                      )
                    : IconButton(
                        icon: Icon(MdiIcons.login),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginScreen(),
                            ),
                          );
                        },
                      ),
              ],
              backgroundColor: Colors.transparent,
              elevation: 0.0,
            ),
          ),
          Positioned(
            key: _keyRed,
            left: 0,
            top: screenHeight * 0.2,
            right: 0,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.white,
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: screenHeight * 0.1),
                    Text(
                      "91.6Mhz",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
                    ),
                    Column(
                      children: [
                        SizedBox(height: screenHeight * 0.03),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: AppColors.audioBulishBackground,
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              StreamBuilder(
                                  stream: _flutterRadioPlayer.isPlayingStream,
                                  initialData: widget.playerState,
                                  builder: (BuildContext context,
                                      AsyncSnapshot<String> snapshot) {
                                    String returnData = snapshot.data;
                                    print("object data: " + returnData);
                                    switch (returnData) {
                                      case FlutterRadioPlayer
                                          .flutter_radio_stopped:
                                        return FlatButton(
                                            shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                  style: BorderStyle.none),
                                            ),
                                            child: Row(
                                              children: [
                                                Icon(Icons.headset,
                                                    color: AppColors
                                                        .audioBlueBackground),
                                                Text(
                                                  " Listening",
                                                  style: TextStyle(
                                                      color: AppColors
                                                          .audioBlueBackground),
                                                ),
                                              ],
                                            ),
                                            onPressed: () async {
                                              await initRadioService();
                                            });
                                        break;
                                      case FlutterRadioPlayer
                                          .flutter_radio_loading:
                                        return FlatButton(
                                            shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                  style: BorderStyle.none),
                                            ),
                                            child: Row(
                                              children: [
                                                Icon(MdiIcons.timerSandEmpty,
                                                    color: AppColors
                                                        .audioBlueBackground),
                                                Text(
                                                  " Loading",
                                                  style: TextStyle(
                                                      color: AppColors
                                                          .audioBlueBackground),
                                                ),
                                              ],
                                            ),
                                            onPressed: null);
                                      case FlutterRadioPlayer
                                          .flutter_radio_error:
                                        return FlatButton(
                                            shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                  style: BorderStyle.none),
                                            ),
                                            child: Row(
                                              children: [
                                                Icon(MdiIcons.timerSandEmpty,
                                                    color: AppColors
                                                        .audioBlueBackground),
                                                Text(
                                                  " Retry",
                                                  style: TextStyle(
                                                      color: AppColors
                                                          .audioBlueBackground),
                                                ),
                                              ],
                                            ),
                                            onPressed: () async {
                                              await initRadioService();
                                            });
                                      default:
                                        return Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              IconButton(
                                                onPressed: () async {
                                                  print("button press data: " +
                                                      snapshot.data.toString());
                                                  await _flutterRadioPlayer
                                                      .playOrPause();
                                                },
                                                icon: snapshot.data ==
                                                        FlutterRadioPlayer
                                                            .flutter_radio_playing
                                                    ? Icon(
                                                        Icons.pause,
                                                        color: AppColors
                                                            .audioBlueBackground,
                                                      )
                                                    : Icon(
                                                        Icons.play_arrow,
                                                        color: AppColors
                                                            .audioBlueBackground,
                                                      ),
                                              ),
                                              IconButton(
                                                  onPressed: () async {
                                                    await _flutterRadioPlayer
                                                        .stop();
                                                  },
                                                  icon: Icon(
                                                    Icons.stop,
                                                    color: AppColors
                                                        .audioBlueBackground,
                                                  ))
                                            ]);
                                        break;
                                    }
                                  }),
                              Lottie.asset(
                                'asset/widget/LoadingRadio.json',
                                width: screenWidth * 0.5,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: screenHeight * 0.05),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: screenHeight * 0.12,
            left: (screenWidth - (screenWidth * 0.33)) / 2,
            right: (screenWidth - (screenWidth * 0.33)) / 2,
            height: screenHeight * 0.16,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.white, width: 2),
                color: AppColors.audioBulishBackground,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Config.logoHomeScreen != null
                    ? Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 5),
                          image: DecorationImage(
                            image: NetworkImage(Config.logoHomeScreen),
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    : Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
              ),
            ),
          ),
          sizeBox != 0
              ? Positioned(
                  top: (screenHeight * 0.2) + (sizeBox + 10.0),
                  left: 0,
                  right: 0,
                  height: screenHeight - (sizeBox + position) - 10,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0)),
                      border: Border.all(color: Colors.white, width: 2),
                      color: AppColors.background,
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Content(
                            title: "Berita UMSU FM",
                            press: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AllBerita(),
                              ),
                            ),
                            tampil: ListBerita(),
                            textAction: "Read more",
                          ),
                          Content(
                            title: "Endorsment",
                            press: () => null,
                            tampil: ListEndorsment(),
                            textAction: "",
                          ),
                          SizedBox(height: screenHeight * 0.12)
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
          currentIndex: _currentIndex,
          selectedItemColor: AppColors.audioBlueBackground,
          unselectedItemColor: Colors.blueGrey,
          elevation: 1,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
            _navigateToScreens(_currentIndex);
          },
          items: [
            BottomNavigationBarItem(
              icon: new Icon(MdiIcons.broadcast),
              title: new Text('Ruang Dengar'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(MdiIcons.radio),
              title: new Text('Daftar Program'),
            )
          ]),
      floatingActionButton:
          user != null ? _getFloatingActionButton() : Container(),
    );
  }

  void _navigateToScreens(int index) {
    switch (index) {
      case 0:
        // do something
        break;
      case 1:
        user != null
            ? Navigator.push(
                context, MaterialPageRoute(builder: (context) => AcaraScreen()))
            : Navigator.push(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
        break;
    }
  }

  Widget _getFloatingActionButton() {
    return SpeedDialMenuButton(
      //if needed to close the menu after clicking sub-FAB
      isShowSpeedDial: _isShowDial,
      //manually open or close menu
      updateSpeedDialStatus: (isShow) {
        //return any open or close change within the widget
        this._isShowDial = isShow;
      },
      //general init
      isMainFABMini: false,
      mainMenuFloatingActionButton: MainMenuFloatingActionButton(
          mini: false,
          child: Icon(MdiIcons.music),
          onPressed: () {},
          closeMenuChild: Icon(Icons.close),
          backgroundColor: Colors.blueGrey,
          closeMenuForegroundColor: Colors.white,
          closeMenuBackgroundColor: Colors.blueGrey),
      floatingActionButtonWidgetChildren: <FloatingActionButton>[
        FloatingActionButton(
          heroTag: "request",
          mini: true,
          child: Icon(MdiIcons.chatOutline),
          onPressed: () {
            _isShowDial = !_isShowDial;
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => RequestScreen()));
          },
          backgroundColor: Colors.blueGrey,
        ),
        FloatingActionButton(
          heroTag: "whastapp",
          mini: true,
          child: Icon(MdiIcons.whatsapp),
          onPressed: () {
            _isShowDial = !_isShowDial;
            _launchURL(Config.waAddress);
          },
          backgroundColor: Colors.blueGrey,
        ),
        FloatingActionButton(
          heroTag: "share",
          mini: true,
          child: Icon(MdiIcons.shareVariant),
          onPressed: () {
            _isShowDial = !_isShowDial;
            shareApp();
          },
          backgroundColor: Colors.blueGrey,
        ),
      ],
      isSpeedDialFABsMini: true,
      paddingBtwSpeedDialButton: 30.0,
    );
  }

  void _launchURL(String _url) async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';

  Future<void> shareApp() async {
    final RenderBox box = context.findRenderObject();
    await Share.share(
      'Assalamu\'alaikum,\n\nKamu tahu gak sih sekarang UMSU FM sudah punya aplikasi mobile?\n\nNih aku kasih tahu, kamu tuh sekarang sudah bisa download aplikasinya di PlayStore & AppStore.',
      sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
}
