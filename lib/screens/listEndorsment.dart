import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:radio/api_service.dart';
import 'package:radio/screens/detailEndors.dart';
import 'package:radio/widgets/grid_endorse.dart';
import 'package:radio/models/endorsment.dart' as endorsModel;

class ListEndorsment extends StatefulWidget {
  @override
  _ListEndorsmentState createState() => _ListEndorsmentState();
}

class _ListEndorsmentState extends State<ListEndorsment> {
  APIService apiService;
  final user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    apiService = new APIService();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          Container(
            height: 150,
            child: Row(
              children: [
                _endorsList(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _endorsList() {
    return new FutureBuilder(
      future: apiService.getEndorse(),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<endorsModel.ModelEndorsment>> model,
      ) {
        if (model.hasData) {
          return _buildEndorsList(model.data);
        }

        return Container(
          height: 120,
          child: Lottie.asset(
            'asset/widget/Loading.json',
            width: 80,
            height: 80,
          ),
        );
      },
    );
  }

  Widget _buildEndorsList(List<endorsModel.ModelEndorsment> endors) {
    if (endors.length > 0) {
      return ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: endors.length,
        itemBuilder: (context, index) {
          var data = endors[index];
          return EndorseGrid(
            img: data.endorsmentFlayer,
            name: data.endorsmentNama,
            id: data.endorsmentId,
            onPress: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailEndors(endors[index])),
              );
            },
          );
        },
      );
    } else {
      return Center(
        child: Column(
          children: [
            Container(
              child: Lottie.asset(
                'asset/widget/Loading.json',
                width: 80,
                height: 80,
              ),
            ),
          ],
        ),
      );
    }
  }
}
