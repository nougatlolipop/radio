import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:radio/config.dart';
import 'package:http/http.dart' as http;
import 'package:radio/widgets/listTileBerita.dart';
import 'bacaBerita.dart';
import 'package:radio/app_colors.dart' as AppColors;

class AllBerita extends StatefulWidget {
  @override
  _AllBeritaState createState() => _AllBeritaState();
}

class _AllBeritaState extends State<AllBerita> {
  List<dynamic> listBerita = [];
  int _chosenValue;
  List<dynamic> _listDays = [
    {'label': 'All', 'value': 365},
    {'label': 'Hari ini', 'value': 0},
    {'label': 'Kemarin', 'value': 1},
    {'label': 'Minggu Lalu', 'value': 7},
    {'label': 'Bulan Lalu', 'value': 30},
  ];

  Future<void> _getBerita(int interval) async {
    var timeInUse = new DateTime(DateTime.now().year, DateTime.now().month,
        DateTime.now().day - interval);
    final response = await http.get(Config.beritaURL + "&per_page=100");
    final data = json.decode(response.body);
    if (mounted) {
      setState(() {
        for (var i = 0; i < data.length; i++) {
          var selisih =
              DateTime.parse(data[i]['date']).difference(timeInUse).inDays;

          if (selisih <= interval && selisih >= 0) {
            listBerita.add(data[i]);
          }
        }
      });
    }
  }

  @override
  void initState() {
    _getBerita(365);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.audioBlueBackground,
        title: Row(
          children: [
            Expanded(child: Text("Berita")),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
              child: DropdownButton(
                dropdownColor: AppColors.audioBlueBackground,
                underline: SizedBox(),
                hint: Text(
                  "Pilih Filter",
                  style: TextStyle(color: Colors.white),
                ),
                value: _chosenValue,
                items: _listDays.map((value) {
                  return DropdownMenuItem(
                    child: Text(
                      value['label'],
                      style: new TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    value: value['value'],
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    _chosenValue = value;
                    listBerita = [];
                    _getBerita(_chosenValue);
                  });
                },
              ),
            ),
          ],
        ),
      ),
      body: new Column(
        children: <Widget>[
          // Padding(
          //   padding: const EdgeInsets.all(16.0),
          //   child: Row(
          //     children: [
          //       Expanded(
          //         child: Text(
          //           "",
          //           style: TextStyle(
          //             fontFamily: 'avenir',
          //             fontSize: 32,
          //             fontWeight: FontWeight.w900,
          //           ),
          //         ),
          //       ),
          //       Container(
          //         padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          //         decoration: BoxDecoration(
          //             color: Colors.white,
          //             borderRadius: BorderRadius.circular(10)),
          //         child: DropdownButton(
          //           underline: SizedBox(),
          //           hint: Text("Pilih Filter"),
          //           value: _chosenValue,
          //           items: _listDays.map((value) {
          //             return DropdownMenuItem(
          //               child: Text(value['label']),
          //               value: value['value'],
          //             );
          //           }).toList(),
          //           onChanged: (value) {
          //             setState(() {
          //               _chosenValue = value;
          //               listBerita = [];
          //               _getBerita(_chosenValue);
          //             });
          //           },
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          Expanded(
            child: listBerita.length > 0
                ? ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: listBerita.length,
                    itemBuilder: (context, index) {
                      Map wppost = listBerita[index];
                      var imageURL = wppost['_embedded']['wp:featuredmedia'][0]
                              ['source_url']
                          .toString();
                      var author =
                          wppost['_embedded']['author'][0]['name'].toString();
                      return ListTileBerita(
                        title: wppost['title']['rendered'].toString(),
                        isi: wppost['excerpt']['rendered'].toString(),
                        createBy: author,
                        date: wppost['date'].toString(),
                        img: imageURL,
                        press: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BacaBerita(
                                  title: wppost['title']['rendered'],
                                  content: wppost['content']['rendered'],
                                  author: wppost['_embedded']['author'][0]
                                          ['name']
                                      .toString(),
                                  date: wppost['date'].toString()),
                            ),
                          );
                        },
                      );
                    },
                  )
                : Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Lottie.asset(
                          'asset/json/notfound.json',
                          width: 150,
                        ),
                        Text("Data Tidak Ditemukan"),
                      ],
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
