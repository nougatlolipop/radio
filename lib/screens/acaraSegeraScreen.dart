import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:lottie/lottie.dart';
import '../models/acara.dart' as acaraModel;
import 'package:radio/widgets/cardAcara.dart';

import '../api_service.dart';

class AcaraSegeraScreen extends StatefulWidget {
  @override
  _AcaraSegeraScreenState createState() => _AcaraSegeraScreenState();
}

class _AcaraSegeraScreenState extends State<AcaraSegeraScreen> {
  APIService apiService;
  bool fav = false;
  final user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    apiService = new APIService();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _acaraList();
  }

  Widget _acaraList() {
    return new FutureBuilder(
      future: apiService.getAcara(user.uid, 0),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<acaraModel.ModelAcara>> model,
      ) {
        if (model.hasData) {
          return _buildAcaraList(model.data, context);
        }

        return Container(
          height: 120,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }

  Widget _buildAcaraList(
      List<acaraModel.ModelAcara> acara, BuildContext context) {
    return new Column(
      children: <Widget>[
        // Padding(
        //   padding: const EdgeInsets.all(16.0),
        //   child: Row(
        //     children: [
        //       Expanded(
        //         child: Text(
        //           "Filter",
        //           style: TextStyle(
        //             fontFamily: 'avenir',
        //             fontSize: 32,
        //             fontWeight: FontWeight.w900,
        //           ),
        //         ),
        //       ),
        // DropdownButton(
        //   hint: Text("Pilih Hari"),
        //   value: _valDay,
        //   items: _listDays.map((value) {
        //     return DropdownMenuItem(
        //       child: Text(value),
        //       value: value,
        //     );
        //   }).toList(),
        //   onChanged: (value) {
        //     print(value);
        //     setState(() {
        //       _valDay = value;
        //     });
        //     apiService.getAcara(user.uid, value).then((val) {
        //       setState(() {
        //         acara = val.toList();
        //       });
        //     });
        //   },
        // ),
        //     ],
        //   ),
        // ),
        acara.length > 0
            ? Expanded(
                child: StaggeredGridView.countBuilder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  crossAxisCount: 2,
                  crossAxisSpacing: 12,
                  mainAxisSpacing: 12,
                  itemCount: acara.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CardAcara(
                      flayer: acara[index].acaraFlayer,
                      namaAcara: acara[index].acaraNama,
                      penyiar: acara[index].acaraPenyiar,
                      mulai: acara[index].acaraJamMulai,
                      akhir: acara[index].acaraJamAkhir,
                      hari: acara[index].acaraHari,
                      fav: acara[index].favorit,
                      press: () {
                        // print(acara[index].acaraId);
                        apiService
                            .addFav(user.uid, acara[index].acaraId)
                            .then((value) {
                          setState(() {});
                        });
                      },
                    );
                  },
                  staggeredTileBuilder: (index) {
                    return new StaggeredTile.fit(1);
                  },
                ),
              )
            : Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.asset(
                      'asset/json/notfound.json',
                      width: 150,
                    ),
                    Text("Data Tidak Ditemukan"),
                  ],
                ),
              ),
      ],
    );
  }
}
