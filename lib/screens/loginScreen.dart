import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../app_colors.dart' as AppColors;
import '../config.dart';
import '../provider/google_sign_in.dart';
import 'playScreen.dart';
import '../widgets/buttonLogin.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.audioBlueBackground,
      body: Center(
        child: Container(
          margin: EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Config.logoHomeScreen != null
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(150.0),
                        child: Image.network(
                          Config.logoHomeScreen,
                          width: 350.0,
                          height: 350.0,
                        ),
                      )
                    : Container(
                        width: 350.0,
                        height: 350.0,
                        child: Center(
                            child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        )),
                      ),
                SizedBox(height: screenHeight * 0.01),
                ChangeNotifierProvider(
                  create: (context) => GoogleSignInProvider(),
                  child: StreamBuilder(
                    stream: FirebaseAuth.instance.authStateChanges(),
                    builder: (context, snapshot) {
                      final provider =
                          Provider.of<GoogleSignInProvider>(context);

                      if (provider.isSigningIn) {
                        return buildLoading();
                      } else if (snapshot.hasData) {
                        return Column(
                          children: [
                            Text(
                              "Welcome",
                              style: TextStyle(
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            Text(
                              "Kawula Muda UMSU FM",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                            ),
                            SizedBox(height: screenHeight * 0.1),
                            ButtonLogin(
                              screenWidth: screenWidth,
                              label: "Ke Ruang Dengar",
                              icon: "broadcast",
                              press: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => PlayScreen()));
                              },
                            ),
                          ],
                        );
                      } else {
                        if (Platform.isAndroid) {
                          return Column(
                            children: [
                              Text(
                                "Sign Up",
                                style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              Text(
                                "it's easier to sign in up now",
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                              SizedBox(height: screenHeight * 0.1),
                              ButtonLogin(
                                screenWidth: screenWidth,
                                label: "Sign In With Google",
                                icon: "google",
                                press: () {
                                  final provider =
                                      Provider.of<GoogleSignInProvider>(context,
                                          listen: false);
                                  provider.login();
                                },
                              ),
                            ],
                          );
                        } else if (Platform.isIOS) {
                          return Column(
                            children: [
                              Text(
                                "Sign Up",
                                style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              Text(
                                "it's easier to sign in up now",
                                style: TextStyle(
                                    fontSize: 16, color: Colors.white),
                              ),
                              SizedBox(height: screenHeight * 0.1),
                              ButtonLogin(
                                screenWidth: screenWidth,
                                label: "Sign In With Google",
                                icon: "google",
                                press: () {
                                  final provider =
                                      Provider.of<GoogleSignInProvider>(context,
                                          listen: false);
                                  provider.login();
                                },
                              ),
                              SizedBox(height: 15),
                              ButtonLogin(
                                screenWidth: screenWidth,
                                label: "Sign In With Apple",
                                icon: "apple",
                                press: () async {
                                  final provider =
                                      Provider.of<GoogleSignInProvider>(context,
                                          listen: false);
                                  provider.loginApple();
                                },
                              ),
                            ],
                          );
                        }
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // Future<void>appleAuth()async{
  //   final result= await _fire
  // }

  Widget buildLoading() => Center(
          child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
      ));
}
