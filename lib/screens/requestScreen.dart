// import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tawk/flutter_tawk.dart';
import 'package:lottie/lottie.dart';
import '../app_colors.dart' as AppColors;

import '../config.dart';

class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  // final user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.audioBlueBackground,
        elevation: 0,
        title: Text("Request Lagu"),
      ),
      body: Tawk(
        directChatLink: Config.tawktoAddress,
        onLoad: () {
          print('Hello Tawk!');
        },
        onLinkTap: (String url) {
          print(url);
        },
        placeholder: Center(
          child: Lottie.asset(
            "asset/widget/Loading.json",
            width: 80,
            height: 80,
          ),
        ),
      ),
    );
  }
}
