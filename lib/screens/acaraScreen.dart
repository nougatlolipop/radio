import 'package:flutter/material.dart';
import '../app_colors.dart' as AppColors;
import 'acaraSegeraScreen.dart';
import 'acaraTayangScreen.dart';

class AcaraScreen extends StatefulWidget {
  @override
  _AcaraScreenState createState() => _AcaraScreenState();
}

class _AcaraScreenState extends State<AcaraScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.audioBlueBackground,
            elevation: 0,
            bottom: const TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(text: "Tayang"),
                Tab(text: "Segera"),
              ],
            ),
            title: Text("List Acara"),
          ),
          body: TabBarView(
            children: [
              AcaraTayangScreen(),
              AcaraSegeraScreen(),
            ],
          ),
        ),
      ),
    );
  }
}
