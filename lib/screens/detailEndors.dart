import 'package:flutter/material.dart';
import 'package:radio/models/endorsment.dart';
import 'package:radio/app_colors.dart' as AppColors;

class DetailEndors extends StatefulWidget {
  ModelEndorsment _endors;
  DetailEndors(ModelEndorsment endors) {
    _endors = endors;
  }
  @override
  State<StatefulWidget> createState() {
    return _DetailEndorsState(_endors);
  }
}

class _DetailEndorsState extends State<DetailEndors> {
  ModelEndorsment _endors;
  _DetailEndorsState(ModelEndorsment endors) {
    _endors = endors;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_endors.endorsmentNama),
        backgroundColor: AppColors.audioBlueBackground,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(height: 15),
            Center(
              child: Container(
                height: 320,
                width: 240,
                child: Hero(
                  tag: _endors.endorsmentId,
                  child: Image.network(
                    _endors.endorsmentFlayer,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.all(15),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // border: Border.all(
                    //   color: Colors.red[500],
                    // ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Text(_endors.endorsmentDeskripsi),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
