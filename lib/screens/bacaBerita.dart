import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:html/parser.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class BacaBerita extends StatelessWidget {
  String content, title, author, date;
  BacaBerita({this.title, this.content, this.author, this.date});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Berita"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    parse(title).documentElement.text,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Text(
                            'By : ' + author + ' ',
                            style: TextStyle(color: Colors.black54),
                          ),
                          Icon(
                            MdiIcons.fromString('clock-outline'),
                            color: _getColorFromHex('cccccc'),
                            size: 15,
                          ),
                          Text(
                            ' ' + date.substring(0, 10),
                            style: TextStyle(color: Colors.black54),
                          ),
                        ],
                      ),
                    ],
                  ),
                  // AdmobBanner(
                  //   adUnitId: ams.getBannerAdId(),
                  //   adSize: AdmobBannerSize.FULL_BANNER,
                  // ),
                ],
              ),
            ),
            HtmlWidget(content),
          ],
        ),
      ),
    );
  }
}

Color _getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
}
