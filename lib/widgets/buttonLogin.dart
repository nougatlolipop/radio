import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ButtonLogin extends StatefulWidget {
  double screenWidth;
  Function press;
  String label, icon;

  ButtonLogin({
    this.screenWidth,
    this.press,
    this.label,
    this.icon,
  });
  @override
  _ButtonLoginState createState() => _ButtonLoginState();
}

class _ButtonLoginState extends State<ButtonLogin> {
  @override
  Widget build(BuildContext context) {
    // final double screenHeight = MediaQuery.of(context).size.height;
    return OutlineButton(
      padding: EdgeInsets.all(15.0),
      color: Colors.red,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50),
      ),
      borderSide: BorderSide(color: Colors.white, width: 2),
      onPressed: widget.press,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            MdiIcons.fromString(widget.icon),
            color: Colors.white,
          ),
          SizedBox(
            width: widget.screenWidth * 0.02,
          ),
          Text(
            widget.label,
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }
}
