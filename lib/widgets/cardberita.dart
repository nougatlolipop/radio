import 'package:flutter/material.dart';
import 'package:html/parser.dart';

class CardBerita extends StatelessWidget {
  CardBerita({
    this.title,
    this.image,
    this.author,
    this.press,
  });

  String title, image, author;
  GestureTapCallback press;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Padding(
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        child: Stack(
          children: [
            Image.network(image, width: 250, height: 120, fit: BoxFit.cover),
            Positioned(
              top: 5,
              left: 5,
              right: 5,
              child: Container(
                width: 300,
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      parse(title).documentElement.text,
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text(
                      "By : " + author,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
