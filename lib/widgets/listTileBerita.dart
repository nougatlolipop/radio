import 'package:flutter/material.dart';
import 'package:html/parser.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ListTileBerita extends StatelessWidget {
  String title;
  String isi;
  String createBy;
  String date;
  String img;
  GestureTapCallback press;
  ListTileBerita(
      {this.title, this.isi, this.createBy, this.date, this.img, this.press});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 16,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: press,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              alignment: Alignment.bottomLeft,
              children: [
                Image.network(
                  img,
                  fit: BoxFit.fitWidth,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    parse(title).documentElement.text,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(3.0, 3.0),
                          blurRadius: 3.0,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        'By : ' + createBy + ' ',
                        style: TextStyle(color: Colors.black54),
                      ),
                      Icon(
                        MdiIcons.fromString('clock-outline'),
                        color: _getColorFromHex('cccccc'),
                        size: 15,
                      ),
                      Text(
                        ' ' + date.substring(0, 10),
                        style: TextStyle(color: Colors.black54),
                      ),
                    ],
                  ),
                  Text(parse(isi).documentElement.text),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Color _getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
}
