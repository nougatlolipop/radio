import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:radio/screens/bacaBerita.dart';
import '../config.dart';
import 'cardberita.dart';

class ListBerita extends StatefulWidget {
  @override
  _ListBeritaState createState() => _ListBeritaState();
}

class _ListBeritaState extends State<ListBerita> {
  List<dynamic> listBerita = new List<ListBerita>();

  Future<void> _getBerita() async {
    final response = await http.get(Config.beritaURL + "&per_page=5");
    final data = json.decode(response.body);
    if (mounted) {
      setState(() {
        listBerita = data;
      });
    }
  }

  @override
  void initState() {
    _getBerita();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return listBerita.length > 0
        ? SizedBox(
            height: 120,
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: listBerita.length,
              itemBuilder: (BuildContext context, int index) {
                Map wppost = listBerita[index];
                var imageURL = wppost['_embedded']['wp:featuredmedia'][0]
                        ['source_url']
                    .toString();
                var author =
                    wppost['_embedded']['author'][0]['name'].toString();
                if (wppost['title']['rendered'] != null) {
                  return CardBerita(
                    image: imageURL,
                    title: wppost['title']['rendered'].toString(),
                    author: author,
                    press: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => BacaBerita(
                              title: wppost['title']['rendered'],
                              content: wppost['content']['rendered'],
                              author: wppost['_embedded']['author'][0]['name']
                                  .toString(),
                              date: wppost['date'].toString()),
                        ),
                      );
                    },
                  );
                }
              },
            ),
          )
        : Lottie.asset(
            'asset/widget/Loading.json',
            width: 80,
            height: 80,
          );
  }
}

Color _getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
}
