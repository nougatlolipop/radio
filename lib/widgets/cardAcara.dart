import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:radio/provider/google_sign_in.dart';
import 'package:radio/screens/loginScreen.dart';
import 'package:radio/widgets/marquee.dart';
import 'package:radio/app_colors.dart' as AppColors;

class CardAcara extends StatefulWidget {
  CardAcara({
    this.flayer,
    this.namaAcara,
    this.penyiar,
    this.hari,
    this.mulai,
    this.akhir,
    this.fav,
    this.press,
  });

  String flayer, namaAcara, penyiar, hari, mulai, akhir;
  int fav;
  Function press;
  @override
  _CardAcaraState createState() => _CardAcaraState();
}

class _CardAcaraState extends State<CardAcara> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0),
      child: Card(
        elevation: 2,
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.network(widget.flayer),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.all(0),
                        child: MarqueeWidget(
                          child: Text(
                            widget.namaAcara,
                            style: TextStyle(fontWeight: FontWeight.bold),
                            overflow: TextOverflow.fade,
                          ),
                        ),
                      ),
                      Text(
                        "Penyiar : " + widget.penyiar,
                        style: TextStyle(fontSize: 12),
                      ),
                      Container(
                        margin: EdgeInsets.all(0),
                        child: MarqueeWidget(
                          direction: Axis.horizontal,
                          child: Text(
                            "${widget.hari.replaceAll(',', ', ')}",
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ),
                      Text(
                        "${widget.mulai.substring(0, 5) + " - " + widget.akhir.substring(0, 5)}",
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              top: 0,
              left: -20,
              child: MaterialButton(
                elevation: 10,
                onPressed: widget.press,
                color: Colors.white,
                textColor: AppColors.menu2Color,
                child: Icon(
                  widget.fav > 0 ? MdiIcons.heart : MdiIcons.heartOutline,
                  size: 20,
                ),
                padding: EdgeInsets.all(5),
                shape: CircleBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
