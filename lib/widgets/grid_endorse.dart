import 'package:flutter/material.dart';
// import 'package:wallet/utils/logo_helper.dart';

class EndorseGrid extends StatelessWidget {
  final String img, name, id;
  Function onPress;
  EndorseGrid({this.img, this.name, this.id, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.only(right: 10),
        height: 160,
        width: 120,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: Color(0xfff1f3f6)),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Hero(
                tag: id,
                child: Image.network(
                  img,
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
