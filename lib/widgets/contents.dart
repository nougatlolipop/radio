import 'package:flutter/material.dart';
import '../app_colors.dart' as AppColors;

class Content extends StatefulWidget {
  String title, textAction;
  Function press;
  Widget tampil;

  Content({
    this.title,
    this.press,
    this.tampil,
    this.textAction,
  });
  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.title,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 16.0,
                    color: Colors.blueGrey,
                  ),
                ),
                TextButton(
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.zero,
                        side: BorderSide(color: Colors.red),
                      ),
                    ),
                  ),
                  child: Text(
                    widget.textAction,
                    style: TextStyle(
                      color: AppColors.audioBlueBackground,
                    ),
                  ),
                  onPressed: widget.press,
                )
              ],
            ),
            SizedBox(height: 10),
            widget.tampil,
          ],
        ),
      ),
    );
  }
}
