import 'dart:io';

import 'package:dio/dio.dart';
import 'package:radio/models/endorsment.dart';
import 'package:radio/models/favorit.dart';

import 'config.dart';
import 'models/acara.dart';
import 'models/config.dart';
import 'models/user.dart';

class APIService {
  Future<User> createUser(
    String uuid,
    String onesignal,
  ) async {
    User model;
    try {
      String url = Config.baseURLApi + Config.urlAddUser;
      print(url);
      var response = await Dio().post(
        url,
        data: {
          "uuid": uuid,
          "useronesignal": onesignal,
        },
        options: new Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded"
          },
        ),
      );

      if (response.statusCode == 200) {
        model = User.fromJson(response.data);
      }
    } on DioError catch (e) {
      print(e);
    }

    return model;
  }

  Future<List<ModelAcara>> getAcara(String uuid, int status) async {
    List<ModelAcara> data = [];

    try {
      String url = Config.baseURLApi +
          Config.urlGetAcara +
          '?uuid=${uuid}&status=${status}';
      var response = await Dio().get(
        url,
        options: new Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          },
        ),
      );

      if (response.statusCode == 200) {
        data = (response.data['data'] as List)
            .map((i) => ModelAcara.fromJson(i))
            .toList();
      }
    } on DioError catch (e) {
      print(e.response);
    }
    return data;
  }

  Future<List<ModelEndorsment>> getEndorse() async {
    List<ModelEndorsment> data = [];

    try {
      String url = Config.baseURLApi + Config.urlGetEndors;
      var response = await Dio().get(
        url,
        options: new Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          },
        ),
      );

      if (response.statusCode == 200) {
        data = (response.data['data'] as List)
            .map((i) => ModelEndorsment.fromJson(i))
            .toList();
      }
    } on DioError catch (e) {
      print(e.response);
    }
    return data;
  }

  Future<List<ModelConfig>> getConfig() async {
    List<ModelConfig> data = [];

    try {
      String url = Config.baseURLApi + Config.urlGetConfig;
      var response = await Dio().get(
        url,
        options: new Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
          },
        ),
      );

      if (response.statusCode == 200) {
        data = (response.data['data'] as List)
            .map((i) => ModelConfig.fromJson(i))
            .toList();
        // print(data);
      }
    } on DioError catch (e) {
      print(e.response);
    }
    return data;
  }

  Future<Favorit> addFav(
    String uuid,
    String acara,
  ) async {
    Favorit model;
    try {
      String url = Config.baseURLApi + Config.urlAddFav;
      print(url);
      var response = await Dio().post(
        url,
        data: {
          "uuid": uuid,
          "acara": acara,
        },
        options: new Options(
          headers: {
            HttpHeaders.contentTypeHeader: "application/x-www-form-urlencoded"
          },
        ),
      );

      if (response.statusCode == 200) {
        model = Favorit.fromJson(response.data);
      }
    } on DioError catch (e) {
      print(e);
    }

    return model;
  }
}
