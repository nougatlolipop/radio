import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class GoogleSignInProvider with ChangeNotifier {
  final googleSignIn = GoogleSignIn();
  GoogleSignInAccount googleAccount;

  bool _isSigningIn;

  GoogleSignInProvider() {
    _isSigningIn = false;
  }

  bool get isSigningIn => _isSigningIn;

  set isSigningIn(bool isSigningIn) {
    _isSigningIn = isSigningIn;
    notifyListeners();
  }

  Future login() async {
    isSigningIn = true;

    this.googleAccount = await googleSignIn.signIn();
    if (this.googleAccount == null) {
      isSigningIn = false;
      return;
    } else {
      final googleAuth = await this.googleAccount.authentication;

      final credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      await FirebaseAuth.instance.signInWithCredential(credential);
      isSigningIn = false;
    }
    notifyListeners();
  }

  Future loginApple() async {
    isSigningIn = true;

    final AuthorizationResult result = await AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [
        Scope.email,
        Scope.fullName,
      ])
    ]);

    switch (result.status) {
      case AuthorizationStatus.authorized:
        final AppleIdCredential _auth = result.credential;
        final OAuthProvider oAuthProvider = new OAuthProvider("apple.com");
        final AuthCredential credential = oAuthProvider.getCredential(
          idToken: String.fromCharCodes(_auth.identityToken),
          accessToken: String.fromCharCodes(_auth.authorizationCode),
        );

        await FirebaseAuth.instance.signInWithCredential(credential);
        isSigningIn = false;
        break;
      case AuthorizationStatus.error:
        print('Sign In Failed ${result.error.localizedDescription}');
        break;
      case AuthorizationStatus.cancelled:
        print('User Canceled');
        break;
    }

    notifyListeners();
  }

  void logout() async {
    this.googleAccount = await googleSignIn.disconnect();
    FirebaseAuth.instance.signOut();
    notifyListeners();
  }
}
