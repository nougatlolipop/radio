class ModelEndorsment {
  String endorsmentId;
  String endorsmentFlayer;
  String endorsmentNama;
  String endorsmentTanggalAwal;
  String endorsmentTanggalAkhir;
  String endorsmentDeskripsi;

  ModelEndorsment({
    this.endorsmentId,
    this.endorsmentFlayer,
    this.endorsmentNama,
    this.endorsmentTanggalAwal,
    this.endorsmentTanggalAkhir,
    this.endorsmentDeskripsi,
  });

  ModelEndorsment.fromJson(Map<String, dynamic> json) {
    endorsmentId = json['endorsmentId'];
    endorsmentFlayer = json['endorsmentFlayer'];
    endorsmentNama = json['endorsmentNama'];
    endorsmentTanggalAwal = json['endorsmentTanggalAwal'];
    endorsmentTanggalAkhir = json['endorsmentTanggalAkhir'];
    endorsmentDeskripsi = json['endorsmentDeskripsi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['endorsmentId'] = this.endorsmentId;
    data['endorsmentFlayer'] = this.endorsmentFlayer;
    data['endorsmentNama'] = this.endorsmentNama;
    data['endorsmentTanggalAwal'] = this.endorsmentTanggalAwal;
    data['endorsmentTanggalAkhir'] = this.endorsmentTanggalAkhir;
    data['endorsmentDeskripsi'] = this.endorsmentDeskripsi;
  }
}
