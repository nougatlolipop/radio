class ModelConfig {
  int configId;
  String configNama;
  String configValue;

  ModelConfig({
    this.configId,
    this.configNama,
    this.configValue,
  });

  ModelConfig.fromJson(Map<String, dynamic> json) {
    configId = int.parse(json['configId']);
    configNama = json['configNama'];
    configValue = json['configValue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['configId'] = this.configId;
    data['configNama'] = this.configNama;
    data['configValue'] = this.configValue;
    return data;
  }
}
