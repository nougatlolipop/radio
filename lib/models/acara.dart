class ModelAcara {
  String acaraId;
  String acaraFlayer;
  String acaraNama;
  String acaraHari;
  String acaraJamMulai;
  String acaraJamAkhir;
  String acaraPenyiar;
  int favorit;

  ModelAcara(
      {this.acaraId,
      this.acaraFlayer,
      this.acaraNama,
      this.acaraHari,
      this.acaraJamMulai,
      this.acaraJamAkhir,
      this.acaraPenyiar,
      this.favorit});

  ModelAcara.fromJson(Map<String, dynamic> json) {
    acaraId = json['acaraId'];
    acaraFlayer = json['acaraFlayer'];
    acaraNama = json['acaraNama'];
    acaraHari = json['acaraHari'];
    acaraJamMulai = json['acaraJamMulai'];
    acaraJamAkhir = json['acaraJamAkhir'];
    acaraPenyiar = json['penyiarNama'];
    favorit = int.parse(json['favorit']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['acaraId'] = this.acaraId;
    data['acaraFlayer'] = this.acaraFlayer;
    data['acaraNama'] = this.acaraNama;
    data['acaraHari'] = this.acaraHari;
    data['acaraJamMulai'] = this.acaraJamMulai;
    data['acaraJamAkhir'] = this.acaraJamAkhir;
    data['penyiarNama'] = this.acaraPenyiar;
    data['favorit'] = this.favorit;
    return data;
  }
}
